<?php
/*
Plugin Name: Email Users on Update of Download for Easy Digital Downloads
Plugin URI: https://wordpress.org/plugins/email-users-on-update-of-download-for-easy-digital-downloads/
Description: Send customers manually an email when an update has been made for a download in Easy Digital Downloads.
Version: 1.1.4
Author: Marcel Pol
Author URI: https://zenoweb.nl
License: GPLv2 or later
Text Domain: email-users-on-update-of-download-for-easy-digital-downloads
Domain Path: /lang/


Copyright 2022 - 2024  Marcel Pol  marcel@timelord.nl

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/


// Plugin Version
define('EDD_EUUD_VER', '1.1.4');


/*
 * Todo List:
 * - Consider adding a mail header with <noreply@domain.com>.
 * - Add support for more than 1000 customers per product.
 *
 */


/*
 * Definitions
 */
define('EDD_EUUD_FOLDER', plugin_basename( __DIR__ ));
define('EDD_EUUD_DIR', WP_PLUGIN_DIR . '/' . EDD_EUUD_FOLDER);
define('EDD_EUUD_URL', plugins_url( '/', __FILE__ ));


if ( is_admin() ) {
	require_once EDD_EUUD_DIR . '/edd-email-user-settingspage.php';
	require_once EDD_EUUD_DIR . '/edd-email-user-email.php';
	require_once EDD_EUUD_DIR . '/edd-email-user-admin-ajax.php';
}


/*
 * Load Language files for dashboard only.
 *
 * @since 1.1.3
 */
function edd_email_on_update_load_lang() {

	load_plugin_textdomain( 'email-users-on-update-of-download-for-easy-digital-downloads', false, plugin_basename( __DIR__ ) . '/lang' );

}
add_action( 'admin_init', 'edd_email_on_update_load_lang' );
