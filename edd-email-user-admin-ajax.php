<?php


if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


/*
 * Add metabox to download product.
 *
 * @since 1.0.0
 */
function edd_product_email_on_update_meta_box() {

	$download_id = get_the_ID();
	$recipients = edd_email_on_update_get_recipients( $download_id );
	$count_recipients = count( $recipients );
	$part_size = 5;
	$total_parts = (int) ceil( $count_recipients / $part_size );
	?>

	<p class="file-download-update-on-email">
		<input type="button" name="file-download-update-on-email" class="file-download-update-on-email button btn btn-primary" value="<?php esc_attr_e('Email Customers', 'email-users-on-update-of-download-for-easy-digital-downloads'); ?>" /><br />
		<label for="file-download-update-on-email" class="text-info file-download-update-on-email"><?php esc_attr_e('Email Customers with a notification of an update of this download.', 'email-users-on-update-of-download-for-easy-digital-downloads'); ?></label>
		<input type="hidden" name="file-download-update-on-email-postid" id="file-download-update-on-email-postid" value="<?php echo (int) $download_id; ?>" />
		<input type="hidden" name="file-download-update-on-email-parts" id="file-download-update-on-email-parts" value="<?php echo (int) $total_parts; ?>" />
	</p>

	<?php
	// Add JavaScript to the Footer so we can do Ajax.
	add_action( 'admin_footer', 'edd_add_email_on_update_add_javascript' );

}


/*
 * Add metabox to download product.
 *
 * @since 1.0.0
 */
function edd_add_email_on_update_meta_box() {
	add_meta_box( 'edd_product_email_on_update', esc_attr__( 'Email on Update', 'email-users-on-update-of-download-for-easy-digital-downloads' ), 'edd_product_email_on_update_meta_box', 'download', 'side', 'low' );
}
add_action( 'add_meta_boxes', 'edd_add_email_on_update_meta_box' );


/*
 * Add JavaScript to the Footer so we can do Ajax.
 *
 * @since 1.0.0
 */
function edd_add_email_on_update_add_javascript() {

	$nonce = wp_create_nonce( 'edd_email_on_update' );
	$ajaxurl = admin_url( 'admin-ajax.php' );
	$postid = get_the_ID();
	?>

	<script>
	jQuery( document ).ready( function( $ ) {

		jQuery( 'p.file-download-update-on-email input.file-download-update-on-email' ).on( 'click', function(event) {

			jQuery( 'p.file-download-update-on-email input.file-download-update-on-email' ).prop('disabled', true);
			jQuery( 'p.file-download-update-on-email input.file-download-update-on-email' ).removeClass( 'button-primary' );
			jQuery( 'p.file-download-update-on-email input.file-download-update-on-email' ).off( 'click' );

			// Enable navigation prompt
			window.onbeforeunload = function() {
				return true;
			};

			var postid = parseInt( jQuery('#file-download-update-on-email-postid').val() );
			var parts = parseInt( jQuery('#file-download-update-on-email-parts').val() );
			var timeout = 0;

			for ( var part = 1; part < (parts + 1); part++ ) {
				timeout = ( part - 1 ) * 10000; // 10 seconds.
				edd_add_email_on_update_submit_part( part, timeout, postid );
			}

			// Remove navigation prompt
			timeout = timeout + 10000; // till after the last run.
			setTimeout(
				function() {
					window.onbeforeunload = null;
				}, ( timeout )
			);

			event.preventDefault();

		});

		/* Do the Submit Event. */
		function edd_add_email_on_update_submit_part( part, timeout, postid ) {
			setTimeout(
				function() {

					// Use an object, arrays are only indexed by integers.
					var edd_email_on_update_ajax_data = {
						permalink: window.location.href,
						action: 'edd_email_on_update',
						part: part,
						security: '<?php echo esc_attr( $nonce ); ?>',
						download_id: postid,
					};

					jQuery.post( '<?php echo esc_url( $ajaxurl ); ?>', edd_email_on_update_ajax_data, function( response ) {
						jQuery( 'label.file-download-update-on-email' ).text( response );
					});

				}, ( timeout )
			);
		}

	});
	</script>

	<?php

}


/*
 * Callback function for handling the Ajax request that is generated from the JavaScript above in edd_add_email_on_update_add_javascript.
 *
 * @return void AJAX callbacks are not supposed to return anything.
 *
 * @since 1.0.0
 */
add_action( 'wp_ajax_edd_email_on_update', 'wp_ajax_edd_email_on_update_callback' );
function wp_ajax_edd_email_on_update_callback() {

	if (isset($_POST['download_id'])) {
		$download_id = (int) $_POST['download_id'];
	}

	if ( ! isset( $download_id ) ) {
		die('error');
	}

	if ( ! is_numeric( $download_id ) ) {
		die('error');
	}

	if (isset($_POST['part'])) {
		$part = (int) $_POST['part'];
	}

	if ( ! isset( $part ) ) {
		die('error');
	}

	if ( ! current_user_can( 'edit_post', $download_id ) ) {
		die('error');
	}

	/* Check Nonce */
	$verified = false;
	if ( isset($_POST['security']) ) {
		$verified = wp_verify_nonce( $_POST['security'], 'edd_email_on_update' );
	}
	if ( $verified === false ) {
		// Nonce is invalid.
		esc_html_e('The Nonce did not validate. Please reload the page and try again.', 'email-users-on-update-of-download-for-easy-digital-downloads');
		die();
	}

	$recipients = edd_email_on_update_get_recipients( $download_id );
	$count_recipients = count( $recipients );
	$part_size = 5;
	$total_parts = (int) ceil( $count_recipients / $part_size );
	$success_counter = 0;
	$array_start = ( ( $part - 1 ) * $part_size );
	$array_end = ( $array_start + ( $part_size - 1 ) );

	for ( $count = 0; $count < $part_size; $count++ ) {

		$key = ( $array_start + $count );
		if ( isset( $recipients["$key"] ) ) {

			$return = edd_email_on_update_send_to_customer( $recipients["$key"], $download_id );

			if ( $return === true ) {
				$success_counter++;
			}
		}

	}

	$array_end++; // array starts at 0, human eye starts at 1.

	if ( $array_end >= $count_recipients ) {

		/* translators: Return message after pressing the button. %d is the number of customers that were sent an email. */
		echo sprintf( _n( 'Finished. %d customer was sent an email.', 'Finished. %d customers were sent an email.', $count_recipients, 'email-users-on-update-of-download-for-easy-digital-downloads' ), $count_recipients );

	} else {

		esc_html_e('Please do not leave this page until sending emails is finished.', 'email-users-on-update-of-download-for-easy-digital-downloads'); echo ' ';
		/* translators: Return message after pressing the button. %1$d / %2$d is the number (of the total) of customers that were sent an email.  */
		echo sprintf( esc_html__( 'Sending emails %1$d / %2$d .', 'email-users-on-update-of-download-for-easy-digital-downloads' ), (int) $array_end, (int) $count_recipients );

	}

	die();

}


/*
 * Gets a list of customers for a download product.
 *
 * @param int $download_id the ID of the download for sale
 * @return array list of customers that made a completed sale.
 *
 * @since 1.0.0
 */
function edd_email_on_update_get_recipients( $download_id ) {

	if ( ! function_exists( 'edd_get_orders' ) ) {
		esc_html_e('This plugin needs EDD v3.0 or higher.', 'email-users-on-update-of-download-for-easy-digital-downloads');
		return array();
	}

	$key = 'edd_email_on_update_get_recipients_' . (int) $download_id;
	$recipients = get_transient( $key );

	if ( ( false === $recipients ) || ( count( $recipients ) === 0 ) ) {

		$orders = edd_get_orders( array(
			'product_id' => $download_id,
			'number'     => 10000,
			'status'     => 'complete',
			'type'       => 'sale', // no refunds.
		) );

		$recipients = array();
		foreach ( $orders as $order ) {
			$user_id = $order->user_id;
			$user_object = get_userdata( $user_id );
			// get current email from wp_users, not old email from EDD order.
			$user_email = '';
			if ( isset( $user_object->data ) && isset( $user_object->data->user_email ) ) {
				$user_email = $user_object->data->user_email;
			}
			if ( $user_email === '' ) {
				continue; // user is deleted probably.
			}

			$recipients[] = $user_email;

		}

		$recipients = array_unique( $recipients );

		// repack to remove empty fields.
		$filtered_recipients = array();
		foreach ( $recipients as $recipient ) {
			if ( isset( $recipient ) ) {
				$filtered_recipients[] = $recipient;
			}
		}

		set_transient( $key, $filtered_recipients, DAY_IN_SECONDS );

		return $filtered_recipients;

	}

	return $recipients;

}
