<?php


if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


/*
 * Adds an option page Settings > EDD Email Users.
 *
 * @since 1.1.0
 */
function edd_email_on_update_settingspage_hook() {
	add_options_page( esc_html__('EDD Email Users', 'email-users-on-update-of-download-for-easy-digital-downloads'), esc_html__('EDD Email Users', 'email-users-on-update-of-download-for-easy-digital-downloads'), 'manage_options', 'edd-email-users.php', 'edd_email_on_update_settingspage');
}
add_action( 'admin_menu', 'edd_email_on_update_settingspage_hook', 11 );



/*
 * Admin Settings page.
 *
 * @since 1.1.0
 */
function edd_email_on_update_settingspage() {

	if ( ! current_user_can('manage_options') ) {
		die(esc_html__('You need a higher level of permission.', 'email-users-on-update-of-download-for-easy-digital-downloads'));
	}

	$messages = '';
	if ( isset( $_POST['option_page']) && $_POST['option_page'] === 'edd_email_on_update_options' ) {
		$messages = edd_email_on_update_settingspage_formupdate();
	} ?>

	<div class="wrap edd_email_on_update_settingspage_wrapper">

		<div id="icon-edd_email_on_update"><br /></div>
		<h1><?php esc_html_e('Email Users in Easy Digital Downloads', 'email-users-on-update-of-download-for-easy-digital-downloads'); ?></h1>

		<?php
		if ( $messages ) {
			echo '
			<div id="message" class="updated fade notice is-dismissible">' .
				wp_kses_post( $messages ) . '
			</div>';
		} ?>

		<form name="edd_email_on_update_settingspage" class="edd_email_on_update_settingspage" method="post" action="">

		<?php
		settings_fields( 'edd_email_on_update_options' );
		do_settings_sections( 'edd_email_on_update_options' );

		/* Nonce */
		$nonce = wp_create_nonce( 'edd_email_on_update_options_nonce' );
		echo '
			<input type="hidden" id="edd_email_on_update_options_nonce" name="edd_email_on_update_options_nonce" value="' . esc_attr( $nonce ) . '" />';

			?>
			<table class="form-table">
				<tbody>

				<tr>
					<th scope="row"><label for="edd_email_on_update-mail_body"><?php esc_html_e('Mail Body', 'email-users-on-update-of-download-for-easy-digital-downloads'); ?></label></th>
					<td>
						<?php
						$mail_body = wp_kses_post( get_option( 'edd_email_on_update-mail_body', '' ) );
						if ( empty( $mail_body ) ) { // No text set by the user. Use the default text.
							$mail_body = edd_email_on_update_get_mail_body_default_text();
						} ?>

						<textarea name="edd_email_on_update-mail_body" id="edd_email_on_update-mail_body" style="width:400px;height:300px;" class="regular-text"><?php echo esc_textarea( $mail_body ); ?></textarea>
						<br />
						<span class="setting-description">
							<?php esc_html_e('You can set the content of the mail that will be sent. The following tags are supported:', 'email-users-on-update-of-download-for-easy-digital-downloads');
							echo '<br />';

							$mailtags = array( 'download_url', 'website_url', 'login_url', 'purchase_history' );
							$mailtags_count = count($mailtags);
							for ($i = 0; $i < $mailtags_count; $i++) {
								if ($i !== 0) {
									echo ', ';
								}
								echo '%' . $mailtags[$i] . '%';
							}
							echo '.'; ?>
						</span>
					</td>
				</tr>

				<tr>
					<th colspan="2">
						<p class="submit">
							<input type="submit" name="edd_email_on_update_settings_submit" id="edd_email_on_update_settings_submit" class="button-primary" value="<?php esc_attr_e('Save settings', 'email-users-on-update-of-download-for-easy-digital-downloads'); ?>" />
						</p>
					</th>
				</tr>
				</tbody>
			</table>

		</form>

	</div> <!-- wrap -->
	<?php
}


/*
 * Save settings.
 *
 * @return string messages for the user as feedback.
 *
 * @since 1.1.0
 */
function edd_email_on_update_settingspage_formupdate() {

	$messages = '';

	if ( ! current_user_can('manage_options') ) {
		return esc_html__('You need a higher level of permission.', 'email-users-on-update-of-download-for-easy-digital-downloads');
	}

	/* Check Nonce */
	$verified = false;
	if ( isset($_POST['edd_email_on_update_options_nonce']) ) {
		$verified = wp_verify_nonce( $_POST['edd_email_on_update_options_nonce'], 'edd_email_on_update_options_nonce' );
	}
	if ( $verified === false ) {
		// Nonce is invalid.
		$messages .= '<p>' . esc_html__('Nonce check failed. Please try again.', 'email-users-on-update-of-download-for-easy-digital-downloads') . '</p>';
		return $messages;
	}

	$saved = false;
	//if ( WP_DEBUG ) { echo "_POST: "; var_dump($_POST); }

	if ( isset( $_POST['option_page']) && $_POST['option_page'] === 'edd_email_on_update_options' ) {

		if ( isset($_POST['edd_email_on_update-mail_body']) ) {
			$mail_body = wp_kses_post( $_POST['edd_email_on_update-mail_body'] );
			update_option('edd_email_on_update-mail_body', $mail_body);
			$saved = true;
		}

	}

	if ( $saved ) {
		$messages .= '<p>' . esc_html__('Changes saved.', 'email-users-on-update-of-download-for-easy-digital-downloads') . '</p>';
	}

	return $messages;

}



/*
 * Register Settings.
 *
 * @since 1.1.0
 */
function edd_email_on_update_register_settings() {

	register_setting( 'edd_email_on_update_options', 'edd_email_on_update-mail_body', 'strval' ); // string

}
add_action( 'admin_init', 'edd_email_on_update_register_settings' );
