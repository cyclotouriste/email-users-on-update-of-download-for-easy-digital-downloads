<?php


if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


/*
 * Send an email to a single customer of the download product.
 *
 * @param string $recipient Email address of the user, fetched from wp_users.
 * @param int $download_id the ID of the download for sale.
 * @return bool true or false, if the mail was sent to the mail function (no further guarantees).
 *
 * @since 1.0.0
 */
function edd_email_on_update_send_to_customer( $recipient, $download_id ) {

	if ( strlen( $recipient ) === 0 ) {
		return false;
	}

	if ( ! isset( $download_id ) ) {
		return false;
	}

	$blog_name = wp_kses( get_bloginfo('name'), array() );
	$mail_body = edd_email_on_update_get_mail_body( $download_id );

	$subject = '[' . $blog_name . '] ' . esc_html__( 'Update of your download', 'email-users-on-update-of-download-for-easy-digital-downloads' );
	$subject = apply_filters( 'edd_email_users_mail_subject', $subject, $download_id );

	//$headers = "Content-Type: text/plain; charset=UTF-8\r\n"; // Encoding of the mail
	//$headers .= 'From: ' . $blog_name . ' <noreply@' . $blog_url . ">\r\n";

	return wp_mail( $recipient, $subject, $mail_body );

}


/*
 * Get the mail body for the email.
 *
 * @param int $download_id the ID of the download for sale.
 * @return string body text of the mail.
 *
 * @since 1.1.0
 */
function edd_email_on_update_get_mail_body( $download_id ) {

	if ( ! isset( $download_id ) ) {
		return false;
	}

	$info = array();
	$info['download_url'] = get_permalink( $download_id );
	$info['website_url'] = get_bloginfo('wpurl');

	$edd_settings = array();
	if ( function_exists( 'edd_get_settings' ) ) {
		$edd_settings = edd_get_settings();
	}

	$info['login_url'] = $info['website_url'];
	if ( isset( $edd_settings['login_page'] ) ) {
		$login_page_id = (int) $edd_settings['login_page'];
		$login_page = get_permalink( $login_page_id );
		$info['login_url'] = $login_page;
	}
	$info['purchase_history'] = $info['website_url'];
	if ( isset( $edd_settings['purchase_history_page'] ) ) {
		$purchase_history_page_id = (int) $edd_settings['purchase_history_page'];
		$purchase_history_page = get_permalink( $purchase_history_page_id );
		$info['purchase_history'] = $purchase_history_page;
	}

	$mail_body = wp_kses_post( get_option( 'edd_email_on_update-mail_body', '' ) );
	if ( empty( $mail_body ) ) { // No text set by the user. Use the default text.
		$mail_body = edd_email_on_update_get_mail_body_default_text();
	}
	$mail_body = apply_filters( 'edd_email_users_mail_body', $mail_body, $download_id );

	$mailtags = array( 'download_url', 'website_url', 'login_url', 'purchase_history' );
	$mailtags_count = count($mailtags);
	for ($tagnum = 0; $tagnum < $mailtags_count; $tagnum++) {
		$tagname = $mailtags["$tagnum"];
		$mail_body = str_replace('%' . $tagname . '%', $info["$tagname"], $mail_body);
	}

	return $mail_body;

}


/*
 * Get the default mail body for the email.
 *
 * @return string default body text of the mail.
 *
 * @since 1.1.0
 */
function edd_email_on_update_get_mail_body_default_text() {

	/* translators: the %text% tags will be replaced by information specific to the website. */
	$mail_body = esc_html__('
Hello dear customer,

There is an update for your download available.
Please take a look to see what is new:
%download_url%

Have a nice day.
The editors at %website_url%.



How to download an update?

1. At the webshop, log in at %login_url%.
2. Browse to Purchase History at %purchase_history%.
3. Select the file you had bought previously.
4. At the Download section, click on the download link.
5. Now you are downloading.

', 'email-users-on-update-of-download-for-easy-digital-downloads');

	return $mail_body;

}
