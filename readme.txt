=== Email Users on Update of Download for Easy Digital Downloads ===
Contributors: mpol
Tags: easy digital downloads, edd, email, customer email
Requires at least: 4.1
Tested up to: 6.7
Stable tag: 1.1.4
License: GPLv2 or later
Requires PHP: 7.0

Send customers manually an email when an update has been made for a download in Easy Digital Downloads.

== Description ==

Send customers manually an email when an update has been made for a download in Easy Digital Downloads e-commerce plugin.
This plugin will add a postbox with a button to each product admin page.
Pressing that button will send an email to every customer of that product that made a complete order.


= Translations =

Translations can be added very easily through [GlotPress](https://translate.wordpress.org/projects/wp-plugins/email-users-on-update-of-download-for-easy-digital-downloads).
You can start translating strings there for your locale. They need to be validated though, so if there's no validator yet, and you want to apply for being validator (PTE), please post it on the support forum.
I will make a request on make/polyglots to have you added as validator for this plugin/locale.

= Contributions =

This plugin is also available in [Codeberg](https://codeberg.org/cyclotouriste/email-users-on-update-of-download-for-easy-digital-downloads).


== Installation ==

= Installation =

* Install the plugin through the admin page "Plugins".
* Alternatively, unpack and upload the contents of the zipfile to your '/wp-content/plugins/' directory.
* Activate the plugin through the 'Plugins' menu in WordPress.
* That's it.


== Frequently Asked Questions ==

= I want to translate this plugin =

Translations can be added very easily through [GlotPress](https://translate.wordpress.org/projects/wp-plugins/email-users-on-update-of-download-for-easy-digital-downloads).
You can start translating strings there for your locale.
They need to be validated though, so if there's no validator yet, and you want to apply for being validator (PTE), please post it on the support forum.
I will make a request on make/polyglots to have you added as validator for this plugin/locale.



== Screenshots ==

1. Test...


== Changelog ==

= 1.1.4 =
* 2024-11-22
* Remove plugin dependencies, so it can be used with EDD Pro as well.

= 1.1.3 =
* 2024-10-02
* Support translations.
* Use __DIR__ instead of dirname(__FILE__).

= 1.1.2 =
* 2024-03-05
* Fix issue on first run of plugin (thanks montseel).
* Workaround issue in easy-media-gallery plugin (thanks montseel).
* Add plugin dependencies for WP 6.5.
* Better test for direct access of php files.

= 1.1.1 =
* 2023-03-24
* Show notification when this plugin is still busy.

= 1.1.0 =
* 2022-10-20
* Add settings page.
* Add option for mail body with tags.
* Put functions into separate files.

= 1.0.0 =
* 2022-09-09
* Initial release.
